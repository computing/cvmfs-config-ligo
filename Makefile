#!/usr/bin/make -f

# This make file takes care of installing files

all: # nothing to build

# default install target is debian because that's the easist way to
#   set up the 'rules' file.
install: install-debian

install-common:
	mkdir -p $(DESTDIR)/etc/cvmfs/default.d \
	  $(DESTDIR)/etc/cvmfs/config.d \
	  $(DESTDIR)/etc/cvmfs/keys/opensciencegrid.org
	install -D -m 0644 60-osg.conf $(DESTDIR)/etc/cvmfs/default.d
	install -D -m 0644 config-osg.opensciencegrid.org.conf $(DESTDIR)/etc/cvmfs/config.d
	install -D -m 0644 opensciencegrid.org.pub $(DESTDIR)/etc/cvmfs/keys/opensciencegrid.org
	install -D -m 0644 default.local $(DESTDIR)/etc/cvmfs

install-debian: install-common
	mkdir -p $(DESTDIR)/lib/systemd/system
	install -D -m 0644 cvmfs-config\\x2dosg.opensciencegrid.org.automount $(DESTDIR)/lib/systemd/system
	install -D -m 0644 cvmfs-config\\x2dosg.opensciencegrid.org.mount $(DESTDIR)/lib/systemd/system
	install -D -m 0644 cvmfs-ligo.osgstorage.org.automount $(DESTDIR)/lib/systemd/system
	install -D -m 0644 cvmfs-ligo.osgstorage.org.mount $(DESTDIR)/lib/systemd/system
	install -D -m 0644 cvmfs-oasis.opensciencegrid.org.automount $(DESTDIR)/lib/systemd/system
	install -D -m 0644 cvmfs-oasis.opensciencegrid.org.mount $(DESTDIR)/lib/systemd/system

# assume DESTDIR=$RPM_BUILD_ROOT is passed in
install-redhat: install-common
