# cvmfs-config-osg

## CVMFS configuration package for the Open Science Grid

This is the source for the CernVM File System (CVMFS) configuration
files for the Open Science Grid (OSG).  It includes packaging for
Redhat and Debian.

### Redhat

End-user documentation for Redhat systems can be found
[here](https://twiki.grid.iu.edu/bin/view/Documentation/Release3/InstallCvmfs).

### Debian

Create a file named `/etc/apt/sources.list.d/cvmfs.list` with these contents
```
deb http://omen.phys.uwm.edu/software/cvmfs ligo main
```
Then install CVMFS with the following command:
```
curl -s http://omen.phys.uwm.edu/keys/nemo.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install cvmfs cvmfs-x509-helper
```
You should now be able to run
```
ligo-proxy-init albert.einstein
ls /cvmfs/ligo.osgstorage.org
```
The configuration installed during these steps is only appropriate for the
light traffic one might expect from single clients. LIGO computing site
adminstrators should follow [these instructions](https://wiki.ligo.org/LVCcomputing/SiteConfiguration).